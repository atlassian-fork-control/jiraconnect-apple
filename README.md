# JIRA Mobile Connect for iOS 8+
[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage) [![Version](http://img.shields.io/cocoapods/v/JIRAMobileConnect.svg)](http://cocoapods.org/?q=JIRAMobileConnect) [![Platform](http://img.shields.io/cocoapods/p/JIRAMobileConnect.svg)]() [![License](http://img.shields.io/cocoapods/l/JIRAMobileConnect.svg)](https://bitbucket.org/atlassian/jiraconnect-apple/src/master/LICENSE.md)

JIRA Mobile Connect (JMC) is an iOS library that can be embedded into any iOS app to provide following extra functionality:

*   **Real-time crash reporting**, have users or testers submit crash reports directly to your JIRA instance.
*   **User or tester feedback** views that allow users or testers to create bug reports within your app.
*   **Rich data input**, users can attach and annotate screenshots, leave a voice message, and have their location sent.
*   **Two-way communication with users**, thank your users or testers for providing feedback on your app!

## Changelog

Version | Description
--- | --- 
**2.0.0 Alpha 2** | JIRA Mobile Connect 2 is a full re-write in the Swift programming language. We are making an alpha release available first, so that we can get your feedback on the API and make breaking changes, if necessary. However, as this release is an alpha, it is currently missing lots of features that were in JIRA Mobile Connect 1. _**JIRA Mobile Connect 2.0.0 Alpha 2 does not include:** Crash Reporting, Commenting back and forth, Custom JIRA issue fields, Sending sound files, Sending photos (automatic screenshot IS supported), Allowing user to send anonymously (if passing in a reporter identifier)._ We'd love to hear your feedback. Send it directly to [rcacheaux@atlassian.com](mailto:rcacheaux@atlassian.com).
**1.2.2** | JIRA Mobile Connect 1 should only be used if you are developing for iOS 7 and earlier. You can find it in a separate Mercurial repository here: https://bitbucket.org/atlassian/jiraconnect-ios 

## Requirements

*   iOS 8.0+
*   Xcode 7.0+
*   A JIRA Software instance _(if you don't have an instance, go to [go.atlassian.com/cloud-dev](http://go.atlassian.com/cloud-dev) and sign up for a free Cloud development environment)_
*   [JIRA Mobile Connect Plugin](https://plugins.atlassian.com/plugin/details/322837)  _(this is only required if you are hosting the JIRA Software instance on your own server)_

## Installation

JIRA Mobile Connect can be installed using either the [Carthage](https://github.com/Carthage/Carthage) or [CocoaPods](http://cocoapods.org/) dependency managers:

*   **Carthage** — Add the following code to your Cartfile:
```
    # Require JIRA Mobile Connect 2.0.0-alpha.9
    git "https://bitbucket.org/atlassian/jiraconnect-apple.git" == 2.0.0-alpha.9
```
*   **CocoaPods** — Add the following code to your Podfile:
```
    # Require JIRA Mobile Connect 2.0.0-alpha.9
    pod "JIRAMobileConnect", "2.0.0-alpha.9"    
```

## Configuration

#### JIRA
JIRA Mobile Connect needs to enabled it on a per project basis, otherwise it will not work with your app. Remember, if you are hosting your own JIRA instance, you will need to install the JIRA Mobile Connect plugin on your server before you can enable it.

To enable JIRA Mobile Connect for a project:

1.  Navigate to the desired project > **Project Settings**.
2.  Find the **Settings** section on the page and click **Enable** for the **JIRA Mobile Connect** setting.  
    This will enable the JIRA Mobile Connect plugin for the project, as well as create a user ('jiraconnectuser') in JIRA that is used to create all feedback and crash reports.
3.  To enable the user to create tickets, you must grant it permission to create issues in the project. To do this, grant the 'Create Issues' permission to the 'jiraconnectuser' user. You can do this by adding the user to a group or project role that has the 'Create Issues' permission or grant the permission to the user directly (see [Managing project permissions](https://confluence.atlassian.com/display/AdminJIRACloud/Managing+project+permissions) for help).

#### iOS
Before you can use JIRA Mobile Connect with iOS, you need to identify the JIRA instance that feedback will be sent to. This is done via a JSON file named **JMCTarget**. You'll need to create this **JMCTarget.json** file in your Xcode project and include it in the iOS app's target so it gets bundled with the app.

1.  Right-click your project in Xcode and click **New File...**
2.  Select **Other** in the **iOS** section and click **Next**.
3.  In the **Save As** field, type 'JMCTarget.JSON' then select the desired **Targets**.
4.  Click **Create**.
5.  Add the following code to your new** JMCTarget.JSON** file. Make sure to replace the values with your own:
```
{
  "host": "example-dev.atlassian.net",
  "projectKey": "EXAMPLEKEY",
  "apiKey": "myApiKey"
}
```

_**Note:** Do not prefix the host with `https://`._

## Using JIRA Mobile Connect in your app
### Trigger from the shake-motion event
The shake-motion event is one of the [motion events](https://developer.apple.com/library/ios/documentation/EventHandling/Conceptual/EventHandlingiPhoneOS/motion_event_basics/motion_event_basics.html#//apple_ref/doc/uid/TP40009541-CH6-SW14) that are detected by the device when it is moved.The following instructions will show you how to trigger a feedback flow (i.e. create a JIRA issue) in your app when the user shakes the device.

1.Import the **JIRAMobileConnect** module into your **AppDelegate**:  
*   Swift: `import JIRAMobileConnect`
*   Objective-C: `@import JIRAMobileConnect;`
    
2.Add the following code to your **AppDelegate**:  
Swift:
```
override func motionBegan(motion: UIEventSubtype, withEvent event: UIEvent?) {
  if let settings = try? FeedbackSettings() {
    self.window?.presentFeedbackIfShakeMotion(motion, settings: settings)
  }
}
```
Objective-C:  
```
-(void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
  [self.window presentFeedbackIfShakeMotion:motion
                                 promptUser:NO
                                  issueType:nil
                            issueComponents:nil
                        reporterAvatarImage:nil
                    reporterUsernameOrEmail:nil];
}
```

That's it! There are a few variations on this implementation, which are shown below:

##### Action sheet prompt
To present an action sheet prompt before taking the user to the feedback flow, set **promptUser** to true.

Swift:
```
override func motionBegan(motion: UIEventSubtype, withEvent event: UIEvent?) {
  if let settings = try? FeedbackSettings() {
    self.window?.presentFeedbackIfShakeMotion(motion, promptUser: true, settings: settings)
  }
}
```
Objective-C:
```
-(void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
  [self.window presentFeedbackIfShakeMotion:motion
                                 promptUser:YES
                                  issueType:nil
                            issueComponents:nil
                        reporterAvatarImage:nil
                    reporterUsernameOrEmail:nil];
}
```

##### Reporter details
To pass a string to identify who is sending the feedback, set **reporterUsernameOrEmail** to the reporter's Id string.
_**Important**: The current version does not let the user know his/her ID is being sent. You may want to only use this feature for non-Appstore builds. To do this, just set reporterUsernameOrEmail to 'nil' for release builds._

Swift:
```
override func motionBegan(motion: UIEventSubtype, withEvent event: UIEvent?) {
  if let settings = try? FeedbackSettings(reporterUsernameOrEmail: "someone@awesome.com") {
    self.window?.presentFeedbackIfShakeMotion(motion, promptUser: true, settings: settings)
  }
}
```
Objective-C:
```
-(void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
  [self.window presentFeedbackIfShakeMotion:motion
                                 promptUser:YES
                                  issueType:nil
                            issueComponents:nil
                        reporterAvatarImage:nil
                    reporterUsernameOrEmail:@"someone@awesome.com"];
}
```

##### Avatar image
To provide the user's avatar image, set **reporterAvatarImage** as shown below.

Swift:
```
override func motionBegan(motion: UIEventSubtype, withEvent event: UIEvent?) {
  let avatarCGImage = UIImage(named: "minime")!.CGImage
  if let settings = try? FeedbackSettings(reporterAvatarImage: avatarCGImage, reporterUsernameOrEmail: "someone@awesome.com") {
    self.window?.presentFeedbackIfShakeMotion(motion, promptUser: true, settings: settings)
  }
}
```
Objective-C:
```
-(void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
  UIImage * avi = [UIImage imageNamed:@"minime"];
  [self.window presentFeedbackIfShakeMotion:motion
                                 promptUser:YES
                                  issueType:nil
                            issueComponents:nil
                        reporterAvatarImage:avi.CGImage
                    reporterUsernameOrEmail:@"someone@awesome.com"];
}
```


### Programmatic trigger

You can also trigger a feedback flow in your app from other events, for example, a button that is tapped in the app. This section shows you how to implement this.

1.Import the **JIRAMobileConnect** module into your **AppDelegate**:

*   Swift: `import JIRAMobileConnect`
*   Objective-C: `@import JIRAMobileConnect;`
    
2.Call `presentFeedback()` on any view controller to present feedback:

Swift:
```
if let settings = try? FeedbackSettings() {
  currentViewController.presentFeedback(settings: settings)
}
```
Objective-C:
```
[currentViewController presentFeedbackWithPromptUser:NO
                                           issueType:nil
                                     issueComponents:nil
                                 reporterAvatarImage:nil
                             reporterUsernameOrEmail:nil];
```

That's it! Just like the shake-motion trigger, there are a few variations on this implementation, which are shown below:

##### Action sheet prompt 

To present an action sheet prompt before taking the user to the feedback flow, set **promptUser** to true.

Swift:
```
if let settings = try? FeedbackSettings() {
  currentViewController.presentFeedback(promptUser: true, settings: settings)
}
```
Objective-C:
```
currentViewController presentFeedbackWithPromptUser:YES
                                           issueType:nil
                                     issueComponents:nil
                                 reporterAvatarImage:nil
                             reporterUsernameOrEmail:nil];
```
    
##### Reporter details

To pass a string to identify who is sending the feedback, set **reporterUsernameOrEmail** to the reporter's Id string:  
_**Important**: The current version does not let the user know his/her ID is being sent. You may want to only use this feature for non-Appstore builds. To do this, just set reporterUsernameOrEmail to 'nil' for release builds._

Swift:
```
if let settings = try? FeedbackSettings(reporterUsernameOrEmail: "someone@awesome.com") {
  currentViewController.presentFeedback(promptUser: true, settings: settings)
}
```
Objective-C:
```
[currentViewController presentFeedbackWithPromptUser:YES
                                           issueType:nil
                                     issueComponents:nil
                                 reporterAvatarImage:nil
                             reporterUsernameOrEmail:@"someone@awesome.com"];
```

##### Avatar image

To provide the user's avatar image, set **reporterAvatarImage** as shown below:

Swift:
```
let avatarCGImage = UIImage(named: "minime")!.CGImage
if let settings = try? FeedbackSettings(reporterAvatarImage: avatarCGImage, reporterUsernameOrEmail: "someone@awesome.com") {
  currentViewController.presentFeedback(promptUser: true, settings: settings)
}
```
Objective-C:
```
UIImage * avi = [UIImage imageNamed:@"minime"];
[currentViewController presentFeedbackWithPromptUser:YES
                                           issueType:nil
                                     issueComponents:nil
                                 reporterAvatarImage:avi.CGImage
                             reporterUsernameOrEmail:@"someone@awesome.com"];
```

## Need Help?

If you have any questions regarding JIRA Mobile Connect, please ask on [Atlassian Answers](https://answers.atlassian.com/tags/jira-mobile-connect/).

## License

Copyright 2011-2015 Atlassian Software.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use these files except in compliance with the License. You may obtain a copy of the License at [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0).

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.