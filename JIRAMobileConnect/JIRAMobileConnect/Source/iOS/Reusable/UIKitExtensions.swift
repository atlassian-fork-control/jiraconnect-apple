//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

// MARK: UIView Screenshot extension
extension UIView {
  public func takeScreenshot() -> UIImage {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0)
    self.drawViewHierarchyInRect(self.bounds, afterScreenUpdates: false)
    let screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return screenshotImage
  }
  
  public func takeScreenshotWithFlashAnimation(onComplete: (UIImage)->()) {
    var takeScreenshotAction: TakeScreenshotAction? = TakeScreenshotAction(application: UIApplication.sharedApplication())
    takeScreenshotAction!.completionBlock = {
      dispatch_async(dispatch_get_main_queue()) {
        onComplete(takeScreenshotAction!.screenshotImage)
        takeScreenshotAction = nil // Break retain cycle.
      }
    }
    takeScreenshotAction!.start()
  }
  
  func createScreenshotFlashView() -> UIView {
    let flashView = UIView()
    flashView.backgroundColor = UIColor.whiteColor()
    flashView.frame = bounds
    return flashView
  }
}

// MARK: Localization extensions
extension AlertController {
  static func createAlertSheetController(titleKey: String, messageKey: String) -> AlertController {
    return AlertController(title: titleKey.localizedString, message: messageKey.localizedString, preferredStyle: .ActionSheet)
  }
  
  static func createAlertController(titleKey: String, messageKey: String) -> AlertController {
    return AlertController(title: titleKey.localizedString, message: messageKey.localizedString, preferredStyle: .Alert)
  }
}

extension UIAlertAction {
  convenience init(titleKey: String, style: UIAlertActionStyle, handler: ((UIAlertAction) -> Void)?) {
    self.init(title: titleKey.localizedString, style: style, handler: handler)
  }
}

// MARK: UIAlertController cancel extension
extension UIAlertController {
  func addCancelAction() {
    let cancelActionTitle = "global_cancel_button_title".localizedString
    let cancelAction = UIAlertAction(title: cancelActionTitle, style: .Cancel, handler: nil)
    self.addAction(cancelAction)
  }
  
  func addDefaultAction(titleKey: String, handler: (UIAlertAction)->Void) {
    let defaultAction = UIAlertAction(titleKey: titleKey, style: .Default, handler: handler)
    self.addAction(defaultAction)
  }
}
