//  Copyright © 2016 Atlassian Pty Ltd. All rights reserved.

import CoreGraphics

public var defaultFeedbackSettings: FeedbackSettings?

public struct FeedbackSettings {
  let JIRATarget: JMCTarget
  let issueType: String
  let issueComponents: [String]
  let reporterAvatarImage: CGImage?
  var reporterUsernameOrEmail: String?
  let getReporterInfoAsynchronously: (((String?, NSURL?) -> Void) -> Void)?
  
    public init(JIRATarget: JMCTarget, issueType: String? = nil, issueComponents: [String]? = nil, reporterAvatarImage: CGImage? = nil, reporterUsernameOrEmail: String? = nil, getReporterInfoAsynchronously: (((usernameOrEmail: String?, avatarImageURL: NSURL?) -> Void) -> Void)? = nil) {
    let defaultIssueType = JIRAIssueType.Bug.rawValue
    let defaultIssueComponents = ["iOS"] // TODO: Use different default based on build target platform.
    
    self.JIRATarget = JIRATarget
    self.issueType = (issueType ?? defaultFeedbackSettings?.issueType) ?? defaultIssueType
    self.issueComponents = (issueComponents ?? defaultFeedbackSettings?.issueComponents) ?? defaultIssueComponents
    self.reporterAvatarImage = reporterAvatarImage ?? defaultFeedbackSettings?.reporterAvatarImage
    self.reporterUsernameOrEmail = reporterUsernameOrEmail ?? defaultFeedbackSettings?.reporterUsernameOrEmail
    self.getReporterInfoAsynchronously = getReporterInfoAsynchronously ?? defaultFeedbackSettings?.getReporterInfoAsynchronously
  }
  
  public init(issueType: String? = nil, issueComponents: [String]? = nil, reporterAvatarImage: CGImage? = nil, reporterUsernameOrEmail: String? = nil, getReporterInfoAsynchronously: (((usernameOrEmail: String?, avatarImageURL: NSURL?) -> Void) -> Void)? = nil) throws {
    // TODO: Make this async.
    let target = try defaultFeedbackSettings?.JIRATarget ?? JMCTarget.createTargetFromJSONOnDisk()
    
    self.init(JIRATarget: target, issueType: issueType, issueComponents: issueComponents,
      reporterAvatarImage: reporterAvatarImage, reporterUsernameOrEmail: reporterUsernameOrEmail, getReporterInfoAsynchronously: getReporterInfoAsynchronously)
  }
}

public enum JIRAIssueType: String {
  case Bug = "Bug"
  case Task = "Task"
  case Improvement = "Improvement"
  case Story = "Story"
  case Epic = "Epic"
}
